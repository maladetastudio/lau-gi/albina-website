export type RegionCodes = "ES-CT-L";

export const regionCodes: readonly RegionCodes[] = Object.freeze([
  "ES-CT-L"
]);

export const regionsRegex = /^(ES-CT-L)/;
