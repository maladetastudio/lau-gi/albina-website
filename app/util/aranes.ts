import { $language } from "../appStore";

// Applies the correct aranes translation to the date
export function replaceAranes(date: string) {
    const mondayDate = date.replace(/(lunes|dilluns|monday)/, "deluns");
    const tuesdayDate = date.replace(/(martes|dimarts|tuesday)/, "dimars");
    const wedensdayDate = date.replace(/(miércoles|dimecres|wednesday)/, "dimèrcles");
    const thursdayDate = date.replace(/(jueves|dijous|thursday)/, "dijaus");
    const fridayDate = date.replace(/(viernes|divendres|monday)/, "diuendres");
    const saturdayDate = date.replace(/(sábado|dissabte|saturday)/, "dissabte");
    const sundayDate = date.replace(/(domingo|diumenge|sunday)/, "dimenge");

    const janDate = date.replace(/(enero|gener|january)/, "Gèr");
    const febDate = date.replace(/(febrero|febrer|february)/, "Heruèr");
    const marchDate = date.replace(/(marzo|març|march)/, "Març");
    const aprilDate = date.replace(/(abril|abril|april)/, "Abriu");
    const mayDate = date.replace(/(mayo|maig|may)/, "Mai");
    const juneDate = date.replace(/(junio|juny|june)/, "Junh");
    const julyDate = date.replace(/(julio|juliol|july)/, "Junhsèga");
    const augDate = date.replace(/(agosto|agost|august)/, "Agost");
    const septDate = date.replace(/(septiembre|setembre|september)/, "Seteme");
    const octoDate = date.replace(/(octubre|octubre|october)/, "Octubre");
    const novDate = date.replace(/(noviembre|novembre|november)/, "Noveme");
    const decemDate = date.replace(/(diciembre|desembre|december)/, "Deseme");

    let newDateString;
    if (mondayDate != date) { newDateString = mondayDate; }
    else if (tuesdayDate != date) { newDateString = tuesdayDate; }
    else if (wedensdayDate != date) { newDateString = wedensdayDate; }
    else if (thursdayDate != date) { newDateString = thursdayDate; }
    else if (fridayDate != date) { newDateString = fridayDate; }
    else if (saturdayDate != date) { newDateString = saturdayDate; }
    else if (sundayDate != date) { newDateString = sundayDate; }
    else if (janDate != date) { newDateString = janDate; }
    else if (febDate != date) { newDateString = febDate; }
    else if (marchDate != date) { newDateString = marchDate; }
    else if (aprilDate != date) { newDateString = aprilDate; }
    else if (mayDate != date) { newDateString = mayDate; }
    else if (juneDate != date) { newDateString = juneDate; }
    else if (julyDate != date) { newDateString = julyDate; }
    else if (augDate != date) { newDateString = augDate; }
    else if (septDate != date) { newDateString = septDate; }
    else if (octoDate != date) { newDateString = octoDate; }
    else if (novDate != date) { newDateString = novDate; }
    else if (decemDate != date) { newDateString = decemDate; }
    else newDateString = date;

    return ($language.get() === "oc" ? newDateString : date)
}