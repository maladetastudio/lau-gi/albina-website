import React from "react";
import { useIntl } from "../../i18n";

function BulletinFooter() {
  const intl = useIntl();
  return (
    <section className="section-centered section-context">
      <div className="panel">
        <h2 className="subheader">
          {intl.formatMessage({
            id: "button:education:headline"
          })}
        </h2>

        <ul className="list-inline list-buttongroup-dense">
          <li>
            <a className="secondary pure-button" href="/education/danger-scale">
              {intl.formatMessage({
                id: "button:education:danger-scale:text"
              })}
            </a>
          </li>
          <li>
            <a
              className="secondary pure-button"
              href="/education/avalanche-problems"
            >
              {intl.formatMessage({
                id: "button:education:avalanche-problems:text"
              })}
            </a>
          </li>
          <li>
            <a className="secondary pure-button" href="/education/matrix">
              {intl.formatMessage({
                id: "button:education:eaws-matrix:text"
              })}
            </a>
          </li>
          <li>
            <a
              className="secondary pure-button"
              href="/education/avalanche-sizes"
            >
              {intl.formatMessage({
                id: "button:education:avalanche-sizes:text"
              })}
            </a>
          </li>
          <li>
            <a className="secondary pure-button" href="/education/handbook">
              {intl.formatMessage({
                id: "button:education:handbook:text"
              })}
            </a>
          </li>
        </ul>
      </div>
    </section>
  );
}

export default BulletinFooter;
